defmodule Examples.FishSims.Fish do
  @moduledoc false
  defmacro __using__(_) do
    quote location: :keep do
        defstruct x: 0.0, y: 0.0, angle: 0.0, sim: nil, ann: nil, alive: true, fitness: 0, opts: nil, assign: %{}

        import Examples.FishSims.Fish
        alias Examples.FishSims.Std
        alias Examples.FishSims.Utils
        alias __MODULE__

        def key, do: :fish

        def new({ann, _fitness}, opts) do
          new(ann, opts)
        end
        def new(ann, opts) do
          %__MODULE__{ann: ann, sim: Ann.Simulation.new(ann), opts: opts}
        end

        def step(fish = %__MODULE__{alive: false}, _, _), do: fish

        def mapify(fish) do
          Map.new()
            |> Map.put("x", Float.round(fish.x || 0.0, 3))
            |> Map.put("y", Float.round(fish.y || 0.0, 3))
            |> Map.put("angle", Float.round(fish.angle || 0.0, 3))
        end

        defoverridable [mapify: 1, key: 0]
    end
  end

  def assign(obj, key, val), do: Map.put(obj, :assign, Map.put(obj.assign, key, val))
end
