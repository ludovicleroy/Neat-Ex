defmodule Ann do
  alias Ann.Connection

  @moduledoc "A module for storing and (de)serializing ANNs (artificial neural networks)."
  defstruct input: [], output: [], bias: [], connections: %{}

  @doc """
  Creates a new ANN from the given parameters.

  ## Example:
      iex> Ann.new([0, 1], [2], %{0 => Ann.Connection.new(0, 2, 0.5)}) #connects 0 to 2 with a weight of 0.5 (and an id of 0)
      %Ann{connections: %{0 => Ann.Connection.new(0, 2, 0.5)}, input: [0, 1], output: [2]}
  """
  def new(inputs, outputs, connections \\ %{}) do
    %Ann{input: inputs, output: outputs, connections: connections}
  end

  @doc "Creates a large feedForward network with hidden layers. `hiddenMatrix` is a list where each element represents the number of hidden nodes existing at that layer."
  def newFeedforward(inputs, outputs, hiddenMatrix) do
    max = max(Enum.max(inputs), Enum.max(outputs))
    {connections, _, previous, bias} = Enum.reduce hiddenMatrix, {%{}, max+1, [max+1 | inputs], [max+1]}, fn layerSize, {connections, id, previous, bias} ->
      bia = id + 1
      layer = Enum.map(2..(layerSize+1), fn mod -> #+1 for the extra bias, starts at 2 to exclude it.
        id + mod
      end)
      {connectLayers(previous, layer, connections), id + layerSize+1, [bia | layer], [bia | bias]}
    end
    %Ann{input: inputs, output: outputs, bias: bias, connections: connectLayers(previous, outputs, connections)}
  end

  def connectLayers(inputs, outputs, connections \\ %{}) do
    Enum.reduce(inputs, connections, fn i_id, connections ->
      Enum.reduce(outputs, connections, fn o_id, connections ->
        Map.put(connections, Connection.genCantorId(i_id, o_id), Connection.new(i_id, o_id))
      end)
    end)
  end

  @doc "Creates a new ANN from the given JSON."
  def new(json) do
    {:ok, map} = JSON.decode(json)
    new(map["input"], map["output"],
      Enum.reduce(map["connections"], %{}, fn conn, acc ->
        Map.put(acc, conn["id"], Connection.new(conn["input"], conn["output"], conn["weight"]))
      end)
    )
  end

  @doc "Forms connections between all of an ANN's input and output neurons with weights of 1.0."
  def connectAll(ann) do
    Map.put(ann, :connections,
      Enum.reduce(ann.input, ann.connections, fn input, acc ->
        Enum.reduce(ann.output, acc, fn output, acc ->
          Map.put(acc, Connection.genCantorId(input, output), Connection.new(input, output, 0.0))
        end)
      end)
    )
  end

  @doc "Returns the JSON representation of a given ANN."
  def json(ann) do
    {:ok, json} = JSON.encode(breakdown(ann))
    json
  end
  def saveJson(ann, path) do
    saveData(json(ann), path)
  end
  def breakdown(ann) do
    [input: ann.input, output: ann.output,
      connections: Enum.map(ann.connections, fn {id, conn} ->
        conn |> Map.delete(:__struct__) |> Map.put(:id, id)
      end)
    ]
  end

  @doc "Returns a string with a GraphViz representation of a neural network."
  def toGZ(ann) do
    "digraph ann {\n" <>
    (ann.connections
      |> Enum.map(fn {_id, conn} -> conn end)
      |> Enum.filter(fn conn -> conn.enabled end)
      |> Enum.map_join("\n", fn conn ->
        "    #{conn.input} -> #{conn.output} [label=\" #{Float.round(conn.weight, 3)} \"];"
      end)
    ) <> "\n{ rank=same; #{Enum.join(ann.input, " ")} }" <> "\n{ rank=same; #{Enum.join(ann.output, " ")} }" <> "\n}"
  end

  @doc "Generates the GraphViz representation of the neural network, then saves it to the given path."
  def saveGZ(ann, path) do
    saveData(toGZ(ann), path)
  end

  @doc "Saves a given binary to a given file path. Overwrites data previously exiting in the file."
  def saveData(binary, path) do
    {:ok, file} = File.open path, [:write]
    :ok = IO.binwrite file, binary
    :ok = File.close file
  end
end
