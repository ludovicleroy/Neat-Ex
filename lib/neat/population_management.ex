defmodule Neat.PopulationManagement do
  require Neat

  @doc "Given Neat, species that have not improved for opts.dropoff_age time steps are dropped."
  def kill_dead_species(neat = %Neat{species: species, opts: opts}) do
    species = Enum.filter species, fn {{_, _, {tsi, _}}, _} ->
      if tsi > opts.dropoff_age, do: IO.write "X"
      tsi <= opts.dropoff_age
    end
    Map.put(neat, :species, species)
  end

  @doc "Given Neat, only the best opts.survival_ratio portion of the species is kept (rounding up to the nearest number of members)."
  def kill_lessers(neat = %Neat{species: species, opts: opts}) do
    species = Enum.map species, fn {rep, members} ->
      {
        rep,
        members |> Enum.sort(fn {_, fit1}, {_, fit2} -> fit1 > fit2 end) |> Enum.take(trunc(Float.ceil(length(members) * opts.survival_ratio)))
      }
    end
    Map.put(neat, :species, species)
  end

  @doc "Given Neat, the best neural network is saved as neat.best as an {ann, fitness} tuple. If the population is empty, the previously best ANN is saved as the new seed."
  def store_best_ann(neat) do
    if neat.species == [] do
      IO.puts "\nWARNING: Extinction has occured."
      {oldbest, _} = neat.best
      if oldbest != nil do
        Map.put(neat, :opts, Map.put(neat.opts, :seed, oldbest))
      else
        neat
      end
    else
      Map.put(neat, :best,
        neat.species
          |> Enum.flat_map(fn {_, members} -> members end)
          |> Enum.max_by(fn {_ann, fitness} -> fitness; _ -> 0 end)
        )
    end
  end
end
